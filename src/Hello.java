/*
Online Java - IDE, Code Editor, Compiler

Online Java is a quick and easy tool that helps you to build, compile, test your programs online.
*/

public class Hello
{
    public static void main(String[] args) {
        System.out.println("========================================");
        System.out.println("Welcome to Online IDE!! Happy Coding :)");
        System.out.println("========================================");

        System.out.println( MyAdd(5,3) );
        System.out.println( MyAdd(20,88) );
    }

    //===============================================================================================
    public static int MyAdd(int x, int y)
    {
        return x+y;
    }
    //===============================================================================================
    
}
